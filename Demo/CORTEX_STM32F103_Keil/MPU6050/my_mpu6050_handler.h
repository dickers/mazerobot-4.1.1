#ifndef __MY_MPU6050_HANDLER_H
#define __MY_MPU6050_HANDLER_H

/*
MY_MPU6050_HANDLER_H
Ver 1.0

- Handler raw data from MPU6050
- Returns rotated angles (pitch and yaw) 
*/

#include "MPU6050.h"

#define HANDLE_AccelGyro_Delay		15 / portTICK_PERIOD_MS

#define CALIBRATE_Delay						3 / portTICK_PERIOD_MS
#define CALIBRATE_Circle					500.

typedef float GYRO_Value;
#define GYRO_Value_Member					3

/*
init things
*/
void Handle_AccelGyro_Init(void);

/*
calibrate raw data
*/
long* CALIBRATE_AccelGyro(void);

/*
handler task, take order from Jerry
*/
void vHandle_AccelGyro_Task(void *pvParameters);

/*
calculate angle from raw data
*/
void Handle_AccelGyro_AngleCalculation(GYRO_Value* reAngle, long* calib);

/*
for testing business
*/
void vTest_Queue_Task(void *pvParameters);

#endif /* __MY_MPU6050_HANDLER_H */
