#include "my_mpu6050_handler.h"

#include "Jerry_movement_define.h"

#include "stdlib.h"
#include "stdio.h"
#include "math.h"
#include "string.h"

#include "task.h"
#include "queue.h"
#include "semphr.h"
#include "my_serial.h"

/*
xQueueHandle gyroQueue;
xSemaphoreHandle gyroSema;
*/

/*-----------------------------------------------------------*/

void Handle_AccelGyro_Init(void)
{
	MPU6050_I2C_Init();
	MPU6050_Initialize();
	
}

/*-----------------------------------------------------------*/

long* CALIBRATE_AccelGyro(void)
{
	long*	calibrateValue = NULL;
	s16*	rawValue = NULL;
	int i = 0, j = 0;
	
	rawValue = (s16 *)calloc(6, sizeof(s16));
	calibrateValue = (long *)calloc(6, sizeof(long));
	//println("\nStarting calibration...");
	
	for (i = 0; i < CALIBRATE_Circle; i++)
	{
		MPU6050_GetRawAccelGyro(rawValue);
		for(j = 0; j < 6; j++)
			calibrateValue[j] += rawValue[j];
		vTaskDelay(CALIBRATE_Delay);
	}
	for (j = 0; j < 6; j++)
		calibrateValue[j] /= CALIBRATE_Circle;
	
	//println("Calibration ended");
	free(rawValue);
	return calibrateValue;
}

/*-----------------------------------------------------------*/
TickType_t timeNow, timeBefore;
GYRO_Value* angle_gyro = NULL;
GYRO_Value* angle_accel = NULL;

GYRO_Value* angle = NULL;
void vHandle_AccelGyro_Task(void *pvParameters)
{
	comuPeri *gyroPara = (comuPeri *)pvParameters;
	
	Jerry_Order_Value jerryOrder;

	long* calibrateValue = NULL;
	//float* angle = NULL;
	
	calibrateValue = (long*)calloc(6, sizeof(long));
	angle_gyro = (GYRO_Value*)calloc(3, sizeof(GYRO_Value));
	angle_accel = (GYRO_Value*)calloc(3, sizeof(GYRO_Value));
	
	angle = (GYRO_Value*)calloc(3, sizeof(GYRO_Value));
	
	calibrateValue = CALIBRATE_AccelGyro();
	xSemaphoreGive(gyroPara->sPeri);
	
	while(1)
	{
		//receive order
		xQueueReceive(gyroPara->qPeri, &jerryOrder, 5);
		
		switch(jerryOrder)
		{
			case GYRO_Order_NormalMode:
				Handle_AccelGyro_AngleCalculation(angle, calibrateValue);
				//send to queue
				xQueueSend(gyroPara->qMove, angle, 5);
				
				break;
			case GYRO_Order_Calibrate:
				free(calibrateValue);
				calibrateValue = CALIBRATE_AccelGyro();
				xSemaphoreGive(gyroPara->sPeri);
				
				break;
			default:
				break;
		}

		vTaskDelay(HANDLE_AccelGyro_Delay);
	}
}

void Handle_AccelGyro_AngleCalculation(GYRO_Value* reAngle, long* calib)
{
	int i = 0;
	
	//accel&gyro
	s16 rawValue[6];
	GYRO_Value accel_total_vec = 0;
	
	MPU6050_GetRawAccelGyro(rawValue);
	
	timeNow = xTaskGetTickCount();
	for(i = 0; i < 3; i++)
	{
		angle_gyro[i] += 0.001 * (timeNow - timeBefore) * (rawValue[i+3] - calib[i+3]) / 131.;
		angle_accel[i] = rawValue[i] - calib[i];
	}
	timeBefore = timeNow;
	
	//angle_gyro[0] += angle_gyro[1] * sin(angle_gyro[2] * 180/3.142);
	//angle_gyro[1] -= angle_gyro[0] * sin(angle_gyro[2] * 180/3.142);
	///*
	accel_total_vec = sqrt(angle_accel[0]*angle_accel[0] + angle_accel[1]*angle_accel[1] + angle_accel[2]*angle_accel[2]);
	angle_accel[1] = asin((GYRO_Value)angle_accel[1]/accel_total_vec) * 180/3.142;
	angle_accel[0] = asin((GYRO_Value)angle_accel[0]/accel_total_vec) * 180/3.142;
	angle_accel[2] = asin((GYRO_Value)angle_accel[2]/accel_total_vec) * 180/3.142;
	
	reAngle[0] = angle_gyro[0]*0.9996 + angle_accel[0]*0.0004;
	reAngle[1] = angle_gyro[1]*0.9996 + angle_accel[1]*0.0004;
	reAngle[2] = angle_gyro[2]*0.9996 + angle_accel[2]*0.0004;
}

/*-----------------------------------------------------------*/

/*
void vTest_Queue_Task(void *pvParameters)
{
	periPara *gyroPara = (periPara *)pvParameters;
	xQueueHandle gyroQueue = gyroPara->queue;
	xSemaphoreHandle gyroSema = gyroPara->sem;
	
	int i = 0, j = 0;
	float* queueValue;
	char* serialData;
	
	queueValue = (float*)calloc(3, sizeof(float));
	serialData = (char*)calloc(6, sizeof(char));
	
	//vTaskDelay(5000);
	
	while(xSemaphoreTake(gyroSema, 5) == pdFALSE);
	xSemaphoreGive(gyroSema);
	
	while(1)
	{
		xQueueReceive(gyroQueue, queueValue, 5);
		
		for (i = 0; i < 3; i++)
		{			
			sprintf(serialData, "%.2f", queueValue[i]);
			
			for (j = 0; j < strlen(serialData); j++)
			{
				USART_SendData(USART1, serialData[j]);
				delayTransmit();
			}
			USART_SendData(USART1, '	');
			delayTransmit();
		}
		
		USART_SendData(USART1, '\n');
		delayTransmit();

		vTaskDelay(10);
	}
}
*/
