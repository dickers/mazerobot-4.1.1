#include "adc_handler.h"

#include "Jerry_movement_define.h"

#include "stdlib.h"
#include "stdio.h"
#include "string.h"

#include "task.h"
#include "my_serial.h"
#include "my_blinky.h"

uint32_t* adcValue;

void Handle_ADC_Init(void)
{
	adcValue = (uint32_t*)calloc(ADC_DMA_Chanel_Number, sizeof(uint32_t));
	ADC_DMA_Config(adcValue);
	
}

ADC_Value* Handle_ADC_Calibrate(uint32_t *adcValue)
{
	ADC_Value* calib;
	int i = 0;
	
	calib = (ADC_Value*)calloc(5, sizeof(ADC_Value));
	
	for (i = 0; i < ADC_Calibrate_Cycle; i++)
	{
		calib[ADC_Calibrate_Center] += adcValue[ADC_SideRight] - adcValue[ADC_SideLeft];
		if(calib[ADC_Calibrate_fLeft] < adcValue[ADC_FrontLeft])
			calib[ADC_Calibrate_fLeft] = adcValue[ADC_FrontLeft];
		if(calib[ADC_Calibrate_Left] < adcValue[ADC_SideLeft])
			calib[ADC_Calibrate_Left] = adcValue[ADC_SideLeft];
		if (calib[ADC_Calibrate_Right] < adcValue[ADC_SideRight])
			calib[ADC_Calibrate_Right] = adcValue[ADC_SideRight];
		if (calib[ADC_Calibrate_fRight] < adcValue[ADC_FrontRight])
			calib[ADC_Calibrate_fRight] = adcValue[ADC_FrontRight];
		
		vTaskDelay(3);
	}

	calib[ADC_Calibrate_fRight] /= ADC_Calibrate_Cycle;	
	
	calib[ADC_Calibrate_Left] -= ADC_Offset_Left;
	calib[ADC_Calibrate_Right] -= ADC_Offset_Right;
	
	return calib;
}

void vHandle_ADC_Task(void *pvParameters)
{
	comuPeri *adcPara = (comuPeri *)pvParameters;
	
	Jerry_Order_Value jerryOrder;
	
	ADC_Value* calibrateValue;
		
	ADC_DMA_Start();
	
	//calibrating
	//while(xSemaphoreTake(adcPara->sPeri, 10) == pdFALSE);
	calibrateValue = Handle_ADC_Calibrate(adcValue);
	xSemaphoreGive(adcPara->sPeri);
	xQueueSend(adcPara->qMove, calibrateValue, 5);
	free(calibrateValue);
	
	while(1)
	{
		xQueueReceive(adcPara->qPeri, &jerryOrder, 5);
		
		switch(jerryOrder)
		{
			case ADC_Order_NormalMode:
				xQueueSend(adcPara->qMove, adcValue, 5);
				break;
			case ADC_Order_Calibrate:
				//LED_Debug_Port->ODR |= LED_Debug_Pin3;
				calibrateValue = Handle_ADC_Calibrate(adcValue);
				xSemaphoreGive(adcPara->sPeri);
				xQueueSend(adcPara->qMove, calibrateValue, 5);
				free(calibrateValue);
				//LED_Debug_Port->ODR &= ~LED_Debug_Pin3;
				break;
		}
		
		vTaskDelay(HANDLE_ADC_delay);
	}
}
