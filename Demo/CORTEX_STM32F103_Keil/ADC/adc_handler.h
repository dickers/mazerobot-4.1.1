#ifndef __ADC_HANDLER_H
#define __ADC_HANDLER_H

#include "ADC_DMA.h"

#define HANDLE_ADC_delay						5 / portTICK_PERIOD_MS

/*-----------------------------------------------------------*/

typedef int32_t ADC_Value;
#define ADC_Value_Member						5

/*-----------------------------------------------------------*/

//ADC define
#define ADC_FrontLeft								3
#define ADC_SideLeft								2
#define ADC_SideRight								1
#define ADC_FrontRight							0

/*-----------------------------------------------------------*/
//ADC calibrate
#define ADC_Calibrate_Cycle					100

#define ADC_Calibrate_Center				0
#define ADC_Calibrate_Left					1
#define ADC_Calibrate_Right					2
#define ADC_Calibrate_fLeft					3
#define ADC_Calibrate_fRight				4

/*-----------------------------------------------------------*/
//ADC offset
#define	ADC_Offset_Left							100
#define ADC_Offset_Right						100
#define	ADC_Offset_FLeft				
#define ADC_Offset_FRight				


void Handle_ADC_Init(void);

ADC_Value* Handle_ADC_Calibrate(uint32_t *adcValue);

void vHandle_ADC_Task(void *pvParameters);

#endif /* __ADC_HANDLER_H */
