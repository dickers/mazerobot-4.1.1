#include "memory.h"

void MEMORY_Write(uint16_t data)
{
	FLASH_Unlock();
	
	FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_PGERR | FLASH_FLAG_WRPRTERR);
	
	FLASH_ErasePage(MEMORY_Start_Address);
	
	FLASH_ProgramHalfWord(MEMORY_Start_Address, data);
	
	FLASH_Lock();
}

uint16_t MEMORY_Read(void)
{
	uint16_t data = 0;
	
	data = *(uint32_t*)(MEMORY_Start_Address);
	
	return data;
}
