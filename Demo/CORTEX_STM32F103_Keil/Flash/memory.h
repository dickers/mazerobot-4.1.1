#ifndef __MEMORY_H
#define __MEMORY_H

#include "stm32f10x_flash.h"

#define MEMORY_Start_Address					((uint32_t)0x08008000)

void MEMORY_Write(uint16_t data);

uint16_t MEMORY_Read(void);

#endif /* __MEMORY_H */
