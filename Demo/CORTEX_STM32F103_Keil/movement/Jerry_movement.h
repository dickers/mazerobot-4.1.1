#ifndef __JERRY_MOVEMENT_H
#define __JERRY_MOVEMENT_H

#include "my_mpu6050_handler.h"
#include "adc_handler.h"
#include "encoder.h"
#include "motor_ctrl.h"
#include "memory.h"

#include "Jerry_movement_define.h"

#define Jerry_Movement_Delay						20 / portTICK_PERIOD_MS

typedef struct
{
	ADC_Value adc[ADC_Value_Member];
	GYRO_Value gyro[GYRO_Value_Member];
	ENCODER_Value encoder[ENCODER_Value_Member];
	
	Jerry_Order_Value adcOrder,
										gyroOrder,
										encoderOrder;
} Jerry_Peripheral;

/*
init things
*/
void Jerry_Movemet_Init(void);

/*
create peripheral taskes
*/
void Jerry_Movement_CreateTaskes(void);

/*
check peripheral condition and wait for some peripheral calibrate itself
*/
Jerry_Peripheral* Jerry_Movement_CheckPeripheral(void);

/*
handle peripheral information
*/
void vJerry_Movement_Task(void *pvParameters);

#endif /* __JERRY_MOVEMENT_H */
