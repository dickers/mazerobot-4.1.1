#include "Jerry_movement.h"

#include "Spike_comunicate_define.h"

#include "stdlib.h"
#include "stdio.h"
#include "string.h"

#include "task.h"
#include "queue.h"
#include "semphr.h"

#include "Jerry_jobs.h"
#include "my_serial.h"

comuPeri periGyro;

comuPeri periADC;

comuPeri periEncode;

void Jerry_Movemet_Init(void)
{
	///*
	LED_Debug_Port->ODR |= LED_Debug_Pin2;
	//*/
	
	//peripheral init
	Handle_AccelGyro_Init();
	Handle_ADC_Init();
	encoder_init();
	PWM_Init_Motor();

	//queue init
	periGyro.qMove = xQueueCreate(1, sizeof(float)*3);
	periGyro.qPeri = xQueueCreate(1, sizeof(Jerry_Order_Value));
	
	periADC.qMove = xQueueCreate(1, sizeof(uint32_t)*5);
	periADC.qPeri = xQueueCreate(1, sizeof(Jerry_Order_Value));
	
	periEncode.qMove = xQueueCreate(1, sizeof(float));
	periEncode.qPeri = xQueueCreate(1, sizeof(Jerry_Order_Value));
	
	//semaphores init
	periGyro.sPeri = xSemaphoreCreateBinary();
	periADC.sPeri = xSemaphoreCreateBinary();
	periEncode.sPeri = xSemaphoreCreateBinary();
}

void Jerry_Movement_CreateTaskes(void)
{
	//Jerry's taskes
	xTaskCreate( vHandle_AccelGyro_Task, "Handle_AccelGyro_Task", configMINIMAL_STACK_SIZE, (void *)&periGyro, tskIDLE_PRIORITY + 4, NULL );
	xTaskCreate( vHandle_ADC_Task, "Handle_ADC_Task", configMINIMAL_STACK_SIZE, (void *)&periADC, tskIDLE_PRIORITY + 2, NULL );
	xTaskCreate( vEncoder_task, "Encoder_task", configMINIMAL_STACK_SIZE, (void *)&periEncode, tskIDLE_PRIORITY + 3, NULL );

}

Jerry_Peripheral* Jerry_Movement_CheckPeripheral(void)
{
	Jerry_Peripheral* periData;
	
	periData = (Jerry_Peripheral*)calloc(1, sizeof(Jerry_Peripheral));

	//create taskes
	Jerry_Movement_CreateTaskes();

	//check gyro
	while(xSemaphoreTake(periGyro.sPeri, 10) == pdFALSE);
	xSemaphoreGive(periGyro.sPeri);
	
	//check adc
	while(xSemaphoreTake(periADC.sPeri, 10) == pdFALSE);
	xQueueReceive(periADC.qMove, periData->adc, 5);
	Jerry_Jobs_ADCInit(periData->adc);
	xSemaphoreGive(periADC.sPeri);

	//check encoder
	while(xSemaphoreTake(periEncode.sPeri, 10) == pdFALSE);
	xSemaphoreGive(periEncode.sPeri);
	
	///*
	LED_Debug_Port->ODR &= ~LED_Debug_Pin2;
	//*/
	return periData;
}

void vJerry_Movement_Task(void *pvParameters)
{
	comuPara *comJerry = (comuPara *)pvParameters;
		
	//movement resources
	int findData = 0;
	
	//finder resources
	int* moveData;
	
	//peri resources
	Jerry_Peripheral *periData;
		
	//pid
	
	
	//
	TickType_t time = xTaskGetTickCount();
	
	/*-----------------------------------------------------------*/
	//check peripheral
	periData = Jerry_Movement_CheckPeripheral();
	xSemaphoreGive(comJerry->moveReady);
	
	/*-----------------------------------------------------------*/
	moveData = (int*)calloc(5, sizeof(int));
	xSemaphoreGive(comJerry->moveSem);
  
	/*-----------------------------------------------------------*/
	while(1)
	{
		/*-----------------------------------------------------------*/
		//peripheral communicate
		xQueueReceive(periGyro.qMove, periData->gyro, 2);
		xQueueReceive(periADC.qMove, periData->adc, 2);
		xQueueReceive(periEncode.qMove, periData->encoder, 2);

		//get find infor
		xQueueReceive(comJerry->findQueue, &findData, 5);
		/*-----------------------------------------------------------*/
				
		//gyro handle
		if(Jerry_Jobs_Gyro(periData->gyro[2]))
			moveData[MoveData_Gyro] = TRUE;
		else
			moveData[MoveData_Gyro] = FALSE;
		periData->gyroOrder = GYRO_Order_NormalMode;
		
		/*-----------------------------------------------------------*/
		
		//adc handle
		/*
		if (xTaskGetTickCount() - time > 10000)
		{
			periData->adcOrder = ADC_Order_Calibrate;
			if(xSemaphoreTake(periADC.sPeri, 10) == pdTRUE)
				periData->adcOrder = ADC_Order_NormalMode;
		}
		else
			periData->adcOrder = ADC_Order_NormalMode;
		*/
		moveData[MoveData_Way] = Jerry_Jobs_ADC(periData->adc);
		periData->adcOrder = ADC_Order_NormalMode;
		
		/*-----------------------------------------------------------*/
		
		//encoder handle
		moveData[MoveData_S] = *periData->encoder;
		
		/*-----------------------------------------------------------*/
		
		//Movement handling
		moveData[MoveData_Move] = Jerry_Jobs_MovingHandler(findData, periData);

		/*-----------------------------------------------------------*/
		//peripheral communicate
		xQueueSend(periGyro.qPeri, &periData->gyroOrder, 2);
		xQueueSend(periADC.qPeri, &periData->adcOrder, 2);
		xQueueSend(periEncode.qPeri, &periData->encoderOrder, 2);

		//send move infor
		xQueueSend(comJerry->moveQueue, moveData, 5);
		/*-----------------------------------------------------------*/

		vTaskDelay(Jerry_Movement_Delay);
	}
}
