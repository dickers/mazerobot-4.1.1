#include "Jerry_jobs.h"

#include "Jerry_movement_define.h"
#include "motor_ctrl.h"
#include "stdlib.h"
#include "math.h"
#include "my_serial.h"
int8_t  flag = 1;

/*-----------------------------------------------------------*/

bool Jerry_Jobs_Gyro(float rotateAngle)
{
	if (abs((int)(rotateAngle - firstAngle)) < 5)
		return TRUE;
	return FALSE;
}

/*-----------------------------------------------------------*/

void Rolling_In_The_Deep(float cur_Angle, float pre_Angle, int8_t angle, int8_t *roll_stt)//queueGyro
{
		if(angle != ANGLE_P90 && (cur_Angle - pre_Angle) <= angle)
		{
			PWM_Speed_Motor(1999,250);
			*roll_stt = 2;
		}
		else if(angle == ANGLE_P90 && (cur_Angle - pre_Angle) >= angle)
		{
			PWM_Speed_Motor(250,1999);
			*roll_stt = 2;
		}
		else *roll_stt = 0;
		
}

/*-----------------------------------------------------------*/
TickType_t time;
int Jerry_Jobs_MovingHandler(int command, Jerry_Peripheral* peri)
{
	int status = 0;
	
	switch(command)
	{
		case Move_Straight:
			time = xTaskGetTickCount();
			LED_Battery_Port->ODR &= ~LED_Battery_Pin;
			status = Moving_Straight;
			break;
		case Move_Left:
			if(xTaskGetTickCount() - time > 100)
				status = Moving_Straight;
			else
				status = Moving_Left;
			LED_Battery_Port->ODR |= LED_Battery_Pin;
			break;
		case Move_Right:
			if(xTaskGetTickCount() - time > 100)
				status = Moving_Straight;
			else
				status = Moving_Right;
			LED_Battery_Port->ODR |= LED_Battery_Pin;
			break;
		case Move_Turn_Around:
			if(xTaskGetTickCount() - time > 100)
				status = Moving_Straight;
			else
				status = Moving_Turn_Around;
			LED_Battery_Port->ODR |= LED_Battery_Pin;
			break;
		case Move_Error:
			status = Moving_Error;
			break;
		default:
			break;
	}
	
	return status;
}

/*-----------------------------------------------------------*/
static ADC_Value* adcStandard = NULL;
void Jerry_Jobs_ADCInit(ADC_Value* calibrate)
{
	adcStandard = (ADC_Value*)calloc(6, sizeof(ADC_Value));
	
	adcStandard[ADC_Standard_Center] = calibrate[ADC_Calibrate_Center];
	adcStandard[ADC_Standard_Left] = calibrate[ADC_Calibrate_Left];
	adcStandard[ADC_Standard_Right] = calibrate[ADC_Calibrate_Right];
	adcStandard[ADC_Standard_fLeft] = calibrate[ADC_Calibrate_fLeft];
	adcStandard[ADC_Standard_fRight] = calibrate[ADC_Calibrate_fRight];
	adcStandard[ADC_Standard_HasBothWall] = adcStandard[ADC_Calibrate_Left] + adcStandard[ADC_Calibrate_Right];
}

int Jerry_Jobs_ADC(ADC_Value* adcValue)
{
	uint8_t WayStatus = 0 ;
	
	if(adcValue[ADC_SideLeft] + adcValue[ADC_SideRight] >= adcStandard[ADC_Standard_HasBothWall]) //Has both wall
	{	
		if (adcValue[ADC_FrontLeft]>= adcStandard[ADC_Standard_fLeft] && adcValue[ADC_FrontRight] >= adcStandard[ADC_Standard_fRight]) //end way
			WayStatus = Way_End;
		else
			WayStatus = Way_Straight;
	} 
	else 
	{
		if(adcValue[ADC_SideLeft] >= adcStandard[ADC_Standard_Left])//only has left wall
		{
			if (adcValue[ADC_FrontLeft]>= adcStandard[ADC_Standard_fLeft] && adcValue[ADC_FrontRight] >= adcStandard[ADC_Standard_fRight])// has wall in front
				WayStatus = Way_Turn_Right;
			else 
				WayStatus = Way_Fork_Right;
		}
		else if (adcValue[ADC_SideRight] >= adcStandard[ADC_Standard_Right])// only has right wall
		{
			if (adcValue[ADC_FrontLeft] >= adcStandard[ADC_Standard_fLeft] && ADC_FrontRight >= adcStandard[ADC_Standard_fRight])//has wall in front
				WayStatus = Way_Turn_Left;
			else 
				WayStatus = Way_Fork_Left;
		} 
		else if (adcValue[ADC_SideLeft] < adcStandard[ADC_Standard_Left] && adcValue[ADC_SideRight] < adcStandard[ADC_Standard_Right])
		{
			if (adcValue[ADC_FrontLeft] < adcStandard[ADC_Standard_Left] && adcValue[ADC_FrontRight] < adcStandard[ADC_Standard_Right])// no wall in front
				WayStatus = Way_CrossRoad;
			else 
				WayStatus = Way_Fork_Left_Right;
		}	
		else WayStatus = Way_Error;
	}
	
	return WayStatus;
}

/*-----------------------------------------------------------*/

//variables
//int32_t error_integral = 0; 
int32_t	pre_error = 0;
int32_t Jerry_Jobs_PID(ADC_Value* adcValue)
{
	int32_t error = 0, derivative, correction, rightIR, leftIR;
	uint32_t delay_time = 1;
//	if(flag)
//	{
//		standard_dis = Jerry_Jobs_FindMP(adcValue);
//	}
//	else
//	{
		leftIR = adcValue[ADC_SideLeft];
		rightIR = adcValue[ADC_SideRight];
		
		error = rightIR - leftIR - adcStandard[ADC_Standard_Center];
		//error_integral += error*delay_time;
		derivative = (float)( error- pre_error)/delay_time;
		pre_error = error;
		//correction =  (P*error + I*error_integral + D*derivative)/10;
		correction =  (P*error + D*derivative)/10;
//	}
	
	return correction;
}

/*-----------------------------------------------------------*/
/*-----------------------------------------------------------*/

/*
int Jerry_Jobs_ADC(uint32_t* adcValue)
{
	if(Jerry_Jobs_ADCCheck(adcValue[ADC_FrontLeft], HAVE_Front_Straight_Min, HAVE_Front_Straight_Max)
	&& Jerry_Jobs_ADCCheck(adcValue[ADC_FrontRight], HAVE_Front_Straight_Min, HAVE_Front_Straight_Max))
	{
		//have straight way
		if(Jerry_Jobs_ADCCheck(adcValue[ADC_SideLeft], HAVE_SideLeft_Turn_Min, HAVE_SideLeft_Turn_Max))
		{
			//have turn left
			if(Jerry_Jobs_ADCCheck(adcValue[ADC_SideRight], HAVE_SideRight_Turn_Min, HAVE_SideRight_Turn_Max))
				//have turn right
				return Way_CrossRoad;
			else
				//do not have turn right
				return Way_Fork_Left;
		}
		else
		{
			//do not have turn left
			if(Jerry_Jobs_ADCCheck(adcValue[ADC_SideRight], HAVE_SideRight_Turn_Min, HAVE_SideRight_Turn_Max))
				//have turn right
				return Way_Fork_Right;
			else
				//do not have turn right
				return Way_Straight;
		}
	}
	else
	{
		//do not have straight way
		if(Jerry_Jobs_ADCCheck(adcValue[ADC_FrontLeft], HAVE_Front_End_Min, HAVE_Front_End_Max)
		&& Jerry_Jobs_ADCCheck(adcValue[ADC_FrontRight], HAVE_Front_End_Min, HAVE_Front_End_Max))
		{
			//have end of way
			if(Jerry_Jobs_ADCCheck(adcValue[ADC_SideLeft], HAVE_SideLeft_Turn_Min, HAVE_SideLeft_Turn_Max))
			{
				//have turn left
				if(Jerry_Jobs_ADCCheck(adcValue[ADC_SideRight], HAVE_SideRight_Turn_Min, HAVE_SideRight_Turn_Max))
					//have turn right
					return Way_Fork_Left_Right;
				else
					//do not have turn right
					return Way_Turn_Left;
			}
			else
			{
				//do not have turn left
				if(Jerry_Jobs_ADCCheck(adcValue[ADC_SideRight], HAVE_SideRight_Turn_Min, HAVE_SideRight_Turn_Max))
					//have turn right
					return Way_Turn_Right;
				else
					//do not have turn right
					return Way_End;
			}
		}
	}
	
	return Way_Error;
}

bool Jerry_Jobs_ADCCheck(uint32_t adcValue, int Min, int Max)
{
	if(adcValue > Min && adcValue < Max)
		return TRUE;
	return FALSE;
}
*/
