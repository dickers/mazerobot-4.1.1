#ifndef __JERRY_MOVEMENT_DEFINE_H
#define __JERRY_MOVEMENT_DEFINE_H

#include "FreeRTOS.h"
#include "queue.h"
#include "semphr.h"

//moveData[_]
#define MoveData_Way									0
#define MoveData_Gyro									1
#define MoveData_End									2
#define MoveData_S										3
#define MoveData_Move									4

//moveData[MoveData_Way]
#define Way_Error											0
#define Way_Straight									1				
#define Way_Turn_Left									2
#define Way_Turn_Right								3
#define Way_Fork_Left									4
#define Way_Fork_Right								5
#define Way_Fork_Left_Right						6
#define Way_CrossRoad									7
#define Way_End												8

//moveData[MoveData_Move]
#define Moving_Error									0
#define Moving_Straight								1
#define Moving_Left										2
#define Moving_Right									3
#define Moving_Turn_Around						4

//findData
#define Move_Error										0
#define Move_Straight									1
#define Move_Left											2
#define Move_Right										3
#define Move_Turn_Around							4

/*-----------------------------------------------------------*/

//orders
//adc
#define ADC_Order_NormalMode					1
#define ADC_Order_Calibrate						2

//gyro
#define GYRO_Order_NormalMode					1
#define GYRO_Order_Calibrate					2

//encoder
#define ENCODER_Order_NormalMode			1

typedef int8_t Jerry_Order_Value;

/*-----------------------------------------------------------*/

typedef struct peripheralParameter
{
	xQueueHandle qMove;
	xQueueHandle qPeri;
	xSemaphoreHandle sPeri;
} comuPeri;


#endif /* __JERRY_MOVEMENT_DEFINE_H */
