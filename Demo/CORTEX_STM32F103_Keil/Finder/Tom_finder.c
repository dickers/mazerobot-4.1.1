#include "Tom_finder.h"

#include "Spike_comunicate_define.h"
#include "Jerry_movement_define.h"

#include "stdlib.h"

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

#include "my_serial.h"
#include "my_blinky.h"

void Tom_Finder_Init(void)
{
	//init necessary things
	
	
}

void vTom_Finder_Task(void *pvParameters)
{
	comuPara *comTom = (comuPara *)pvParameters;
	
	//movement resources
	int* moveData;

	//finder resources
	int findData = 0;
	
	/*-----------------------------------------------------------*/
	while(xSemaphoreTake(comTom->moveReady, 20) == pdFALSE);
	xSemaphoreGive(comTom->moveReady);

	/*-----------------------------------------------------------*/
	moveData = (int*)calloc(5, sizeof(int));
	xSemaphoreGive(comTom->findSem);
	
	/*-----------------------------------------------------------*/
	while(1)
	{
		/*-----------------------------------------------------------*/
		//comunicate
		//get movement info
		xQueueReceive(comTom->moveQueue, moveData, 5);
		/*-----------------------------------------------------------*/
		
		/*
		//gyro handle
		if(moveData[MoveData_Gyro] == FALSE)
			LED_Battery_Port->ODR |= LED_Battery_Pin;
		else
			LED_Battery_Port->ODR &= ~LED_Battery_Pin;
		*/

		/*-----------------------------------------------------------*/
		
		//way and move handle
		findData = Tom_Jobs_FindingHandler(moveData);
		
		/*-----------------------------------------------------------*/
		//comunicate
		//send finder info to movement
		xQueueSend(comTom->findQueue, &findData, 5);
		/*-----------------------------------------------------------*/
		
		vTaskDelay(Tom_Finder_Delay);
	}
}
