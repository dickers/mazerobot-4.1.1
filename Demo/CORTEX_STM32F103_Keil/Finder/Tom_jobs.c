#include "Tom_jobs.h"

#include "Jerry_movement_define.h"

int Tom_Jobs_FindWay(int wayStatus)
{
	int8_t Just_the_way_you_are;
	switch(wayStatus)
	{
		case Way_Straight:
			Just_the_way_you_are = Move_Straight;
			break;
		
		case Way_Turn_Left:
			Just_the_way_you_are = Move_Left;
			break;
		
		case Way_Turn_Right:
			Just_the_way_you_are = Move_Right;
			break;
		
		case Way_Fork_Left:
			if(Find_Way_Mode)
				Just_the_way_you_are = Move_Straight;
			else
				Just_the_way_you_are = Move_Left;
				break;
		
		case Way_Fork_Right:
			if(Find_Way_Mode)
				Just_the_way_you_are = Move_Right;
			else
				Just_the_way_you_are = Move_Straight;
			break;
		
		case Way_Fork_Left_Right:
			if (Find_Way_Mode)
				Just_the_way_you_are = Move_Right;
			else
				Just_the_way_you_are = Move_Left;
			break;
		
		case Way_CrossRoad:
			if (Find_Way_Mode)
				Just_the_way_you_are = Move_Right;
			else
				Just_the_way_you_are = Move_Left;
			break;
		
		case Way_End:
			Just_the_way_you_are = Move_Turn_Around;
			break;
		default:
			Just_the_way_you_are = Move_Error;
			break;
	}
	return Just_the_way_you_are;
}

int Tom_Jobs_FindingHandler(int* move)
{
	int result;
	
	switch(move[MoveData_Move])
	{
		case Moving_Straight:
			result = Tom_Jobs_FindWay(move[MoveData_Way]);
			break;
		case Moving_Left:
			result = Move_Left;
			break;
		case Moving_Right:
			result = Move_Right;
			break;
		case Moving_Turn_Around:
			result = Move_Turn_Around;
			break;
		case Moving_Error:
			result = Tom_Jobs_FindWay(move[MoveData_Way]);
			break;
		default:
			break;
	}
	
	return result;
}
