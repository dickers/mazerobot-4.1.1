#ifndef __TOM_FINDER_H
#define __TOM_FINDER_H

//include things

#include "Tom_jobs.h"

#define Tom_Finder_Delay			25 / portTICK_PERIOD_MS

/*
init things
*/
void Tom_Finder_Init(void);

/*
main task
*/
void vTom_Finder_Task(void *pvParameters);

#endif /* __TOM_FINDER_H */
