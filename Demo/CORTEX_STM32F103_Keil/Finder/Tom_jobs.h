#ifndef __TOM_JOBS_H
#define __TOM_JOBS_H

//0 ~ left			1 ~ right
#define Find_Way_Mode			0

/*
Find way replies on turns and end way of  wayStatus
Return command to Jerry_movement to move
wayStatus = status of way
*/
int Tom_Jobs_FindWay(int wayStatus);

/*
From moveStatus (moveData[MoveData_Move]), give the 
approximate command
*/
int Tom_Jobs_FindingHandler(int* moveStatus);

#endif /* __TOM_JOBS_H */
