/* Define to prevent recursive inclusion*/
#ifndef __MY_INIT_H
#define __MY_INIT_H

#include "stm32f10x_rcc.h"

/*
 * Configure the clocks, GPIO and other peripherals as required by the demo.
 */
void prvSetupHardware( void );

#endif /* __MY_INIT_H */
