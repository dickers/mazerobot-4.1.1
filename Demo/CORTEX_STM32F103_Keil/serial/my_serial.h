/* Define to prevent recursive inclusion*/
#ifndef __MY_SERIAL_H
#define __MY_SERIAL_H

/*
MY_SERIAL_H
Ver 1.0

- Send data through seial (USART1)
*/

#include "stm32f10x_usart.h"
#include <stdint.h>

#define SERIAL_Gpio				GPIOA
#define	SERIAL_Rcc_Gpio		RCC_APB2Periph_GPIOA
#define	SERIAL_Rcc_Usart	RCC_APB2Periph_USART1
#define SERIAL_Tx_Pin			GPIO_Pin_9
#define SERIAL_Rx_Pin			GPIO_Pin_10
#define	SERIAL_Usart			USART1

#define SERIAL_Baud				9600

/*******************************************************************************
* Function Name  : SERIAL_UART1_Init
* Description    : init SERIAL_Usart for serial use
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void SERIAL_USART_Init(void);

/*******************************************************************************
* Function Name  : delayTransmit
* Description    : Delay task until serial transmit was completed
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void delayTransmit(void);

/*******************************************************************************
* Function Name  : println
* Description    : send a string line (data + '\n') to serial
* Input          : a string of character
* Output         : None
* Return         : None
*******************************************************************************/
void println(char* data);

/*******************************************************************************
* Function Name  : print
* Description    : send a string (data) to serial
* Input          : a string of character
* Output         : None
* Return         : None
*******************************************************************************/
void print(char* data);
void print_int(int32_t numb);
#endif /* __MY_SERIAL_H */

