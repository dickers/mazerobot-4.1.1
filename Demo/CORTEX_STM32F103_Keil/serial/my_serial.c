#include "my_serial.h"

#include "stm32f10x_gpio.h"
#include "stm32f10x_nvic.h"
#include "stm32f10x_rcc.h"

#include <stdio.h>
#include <stdint.h>
#include <string.h>

void SERIAL_USART_Init()
{
	//infor for uart
	USART_InitTypeDef uartStruct;
	//infor for uart interrupt
	//NVIC_InitTypeDef NVICStruct;
	//infor for gpio init
	GPIO_InitTypeDef GPIOStruct;

	//enable uart1 clock
	RCC_APB2PeriphClockCmd( RCC_APB2Periph_USART1 | RCC_APB2Periph_GPIOA, ENABLE);
	
	//config uart1 rx as input floating
	GPIOStruct.GPIO_Pin = SERIAL_Rx_Pin;
	GPIOStruct.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init( SERIAL_Gpio, &GPIOStruct);
	
	//config uart1 tx as out put
	GPIOStruct.GPIO_Pin = SERIAL_Tx_Pin;
	GPIOStruct.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIOStruct.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(SERIAL_Gpio, &GPIOStruct);
	
	uartStruct.USART_BaudRate = SERIAL_Baud;
	uartStruct.USART_WordLength = USART_WordLength_8b;
	uartStruct.USART_StopBits = USART_StopBits_1;
	uartStruct.USART_Parity = USART_Parity_No;
	uartStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	uartStruct.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	
	USART_Init(SERIAL_Usart, &uartStruct);
	//USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
	
	/*
	NVICStruct.NVIC_IRQChannel = USART1_IRQChannel;
	NVICStruct.NVIC_IRQChannelPreemptionPriority = configLIBRARY_KERNEL_INTERRUPT_PRIORITY;
	NVICStruct.NVIC_IRQChannelSubPriority = 0;
	NVICStruct.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVICStruct);
	*/
	
	USART_Cmd(SERIAL_Usart, ENABLE);
}

void delayTransmit()
{
	while(!USART_GetFlagStatus(USART1, USART_FLAG_TC));
}

void println(char* data)
{
	int i = 0;
	//wait for usart ready
	while(!USART_GetFlagStatus(USART1, USART_FLAG_TC));
	for (i = 0; i < strlen(data); i++)
	{
		USART_SendData(USART1, data[i]);
		while(!USART_GetFlagStatus(USART1, USART_FLAG_TC));
	}
	USART_SendData(USART1, '\n');
	while(!USART_GetFlagStatus(USART1, USART_FLAG_TC));
}

void print(char* data)
{
	int i = 0;
	//wait for usart ready
	while(!USART_GetFlagStatus(USART1, USART_FLAG_TC));
	for (i = 0; i < strlen(data); i++)
	{
		USART_SendData(USART1, data[i]);
		while(!USART_GetFlagStatus(USART1, USART_FLAG_TC));
	}
}
void print_int(int32_t numb)
{
	char char_temp[10];
	
	sprintf(char_temp, "%d", numb);
	print(char_temp);
}
