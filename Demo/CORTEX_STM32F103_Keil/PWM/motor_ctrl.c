#include "motor_ctrl.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_gpio.h"
#define speed_max 1999
#define ratio	0.8
void GPIO_SetBit(GPIO_TypeDef* GPIOx, u16 GPIO_Pin, u8 value)
{
	if(value)
		GPIOx->ODR |= (1UL << GPIO_Pin);
	else
		GPIOx->ODR &= ~(1UL << GPIO_Pin);
}
void RCC_Init_PWM()
{
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
}
void GPIO_Init_PWM()
{
	GPIO_InitTypeDef					 motor_gpio;
	GPIO_InitTypeDef 					 dir;
	
	motor_gpio.GPIO_Mode 	= GPIO_Mode_AF_PP;
	motor_gpio.GPIO_Pin 	= GPIO_Pin_0 | GPIO_Pin_6;
	motor_gpio.GPIO_Speed = GPIO_Speed_50MHz;
	
	GPIO_Init(GPIOA, &motor_gpio);
	
	dir.GPIO_Mode 	= GPIO_Mode_Out_PP;
	dir.GPIO_Pin 		= GPIO_Pin_1 | GPIO_Pin_7;
	dir.GPIO_Speed	= GPIO_Speed_50MHz;
	
	GPIO_Init(GPIOA,&dir);
}
void PWM_Init_Motor()
{
	
	TIM_TimeBaseInitTypeDef	 	 	motor_timer;
	TIM_OCInitTypeDef 				motor_pwm;
	
	RCC_Init_PWM();
	GPIO_Init_PWM();
	// UAC = 18Mhz
	motor_timer.TIM_ClockDivision  		= 0;
	motor_timer.TIM_CounterMode 		= TIM_CounterMode_Up;
	motor_timer.TIM_Period 				= 1999;
	motor_timer.TIM_Prescaler 			=	35;
	
	TIM_ARRPreloadConfig(TIM2, ENABLE);// preload mode
	TIM_Cmd(TIM2, ENABLE); 
	TIM_TimeBaseInit(TIM2, &motor_timer); // thiet lap thong so pwm
	
	TIM_ARRPreloadConfig(TIM3, ENABLE);// preload mode
	TIM_Cmd(TIM3, ENABLE); 
	TIM_TimeBaseInit(TIM3, &motor_timer); // thiet lap thong so pwm
	
	motor_pwm.TIM_Channel 		= TIM_Channel_1;
	motor_pwm.TIM_OCMode 		= TIM_OCMode_PWM1;
	motor_pwm.TIM_OCPolarity 	= TIM_OCPolarity_High;
	motor_pwm.TIM_Pulse 		= 0;
	
	TIM_OCInit(TIM2, &motor_pwm);
	TIM_OCInit(TIM3, &motor_pwm);

}
void PWM_Speed_Motor(int16_t left, int16_t right)
{
	left *= ratio;
	right *= ratio;
	if(left > speed_max)
		left = speed_max;
	else if(left <-speed_max)
		left = -speed_max;
	if(right > speed_max)
		right = speed_max;
	else if(right <-speed_max)
		right = -speed_max;
		
	if(left >= 0)
	{
		GPIO_SetBit(GPIOA,1,0);
		TIM2->CCR1 = left;
	}
	else
	{
		GPIO_SetBit(GPIOA,1,1);
		TIM2->CCR1 = speed_max + left;
	}
	
	if(right >= 0)
	{
		GPIO_SetBit(GPIOA,7,0);
		TIM3->CCR1 = right;
	}
	else
	{
		GPIO_SetBit(GPIOA,7,1);
		TIM3->CCR1 = speed_max + right;
	}
	
}
