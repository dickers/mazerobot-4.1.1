#include "my_blinky.h"

#include "FreeRTOS.h"
#include "Task.h"

void Led_Init()
{
	GPIO_InitTypeDef gpio;

	RCC_APB2PeriphClockCmd( LED_OS_Rcc | LED_Battery_Rcc | LED_Debug_Rcc, ENABLE);
	//os led
	gpio.GPIO_Speed = GPIO_Speed_50MHz;
	gpio.GPIO_Pin = GPIO_Pin_13;
	gpio.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(LED_OS_Port, &gpio);
	LED_OS_Port->ODR &= ~LED_OS_Pin;
	
	//battery checker
	gpio.GPIO_Pin = GPIO_Pin_8;
	GPIO_Init(LED_Battery_Port, &gpio);
	LED_Battery_Port->ODR &= ~LED_Battery_Pin;

	//debug leds
	gpio.GPIO_Pin = GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15;
	GPIO_Init(LED_Debug_Port, &gpio);
	LED_Debug_Port->ODR &= ~LED_Debug_Pin1;
	LED_Debug_Port->ODR &= ~LED_Debug_Pin2;
	LED_Debug_Port->ODR &= ~LED_Debug_Pin3;
	LED_Debug_Port->ODR &= ~LED_Debug_Pin4;
	
}

void vLed_Task(void *pvParameters)
{
	for (;;)
	{
		if ( GPIOC->ODR & LED_OS_Pin )
			LED_OS_Port->ODR &= ~LED_OS_Pin;
		else
			LED_OS_Port->ODR |= LED_OS_Pin;

		vTaskDelay(LED_OS_Blink_Speed);
	}
	
	//vTaskDelete(NULL);
}
