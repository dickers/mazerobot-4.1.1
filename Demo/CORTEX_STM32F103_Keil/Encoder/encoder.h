#ifndef __ENCODER_H
#define __ENCODER_H

#include "FreeRTOS.h"
#include "Task.h"
#include "my_blinky.h"
#include "stm32f10x_exti.h"
#include "stm32f10x_nvic.h"

#define HANDLE_ENCODER_DELAY					10 / portTICK_PERIOD_MS

typedef float ENCODER_Value;
#define ENCODER_Value_Member					1

void GPIO_Configuration(void);
void NVIC_Configuration(void);
void EXTI_Configuration(void);

void encoder_init(void);
void vEncoder_task(void *pvParameters);
void EXTI4_IRQHandler(void);

#endif /* __ENCODER_H */
