#include "encoder.h"

#include "Jerry_movement_define.h"

volatile int demxung=0;

//

void GPIO_Configuration(void) {
	GPIO_InitTypeDef GPIO_InitStructure;
  
  RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOC, ENABLE); 						 

  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_14;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;//GPIO_Mode_IN_FLOATING; 
  GPIO_Init(GPIOC, &GPIO_InitStructure);
	
}
void NVIC_Configuration(void)
{
  NVIC_InitTypeDef NVIC_InitStructure; 

  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);  													
  NVIC_InitStructure.NVIC_IRQChannel = EXTI15_10_IRQChannel;//EXTI15_10_IRQn;     
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;   
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;	      
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
}
void EXTI_Configuration(void)
{
  EXTI_InitTypeDef EXTI_InitStructure;
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE); 
  GPIO_EXTILineConfig(GPIO_PortSourceGPIOC, GPIO_PinSource14);

  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
  EXTI_InitStructure.EXTI_Line = EXTI_Line14;
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  EXTI_Init(&EXTI_InitStructure);
}

void encoder_init(void) {
	GPIO_Configuration();
	NVIC_Configuration();
	EXTI_Configuration();
}

void EXTI4_IRQHandler(void) {
	if ( EXTI_GetITStatus(EXTI_Line14) != RESET ) {
		demxung++;
		EXTI_ClearITPendingBit(EXTI_Line14);
	}				
}

void vEncoder_task(void *pvParameters)
{
	comuPeri *encodePara = (comuPeri *)pvParameters;
	
	Jerry_Order_Value jerryOrder;
	
	float s = 0;
	xSemaphoreGive(encodePara->sPeri);

	while(1)
	{
		xQueueReceive(encodePara->qPeri, &jerryOrder, 5);
		
		switch(jerryOrder)
		{
			case ENCODER_Order_NormalMode:
				s = (demxung/20)*2.6;
				demxung=0;
			
				xQueueSend(encodePara->qMove, &s, 5);
			break;
			default:
			break;
		}

		vTaskDelay(HANDLE_ENCODER_DELAY);
	}
}


