#ifndef __SPIKE_MANAGEMENT_DEFINE_H
#define __SPIKE_MANAGEMENT_DEFINE_H

#include "FreeRTOS.h"
#include "queue.h"
#include "semphr.h"

typedef struct comunicateParameter
{
	xQueueHandle moveQueue;
	xSemaphoreHandle moveSem;
	xSemaphoreHandle moveReady;
	xQueueHandle findQueue;
	xSemaphoreHandle findSem;
} comuPara;

#endif /* __SPIKE_MANAGEMENT_DEFINE_H */
