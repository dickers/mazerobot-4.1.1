/*
Ver 1.1
*/

/* Standard includes. */
#include <stdio.h>

/* Scheduler includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

/* Library includes. */
#include "my_init.h"
#include "my_blinky.h"
#include "my_serial.h"
#include "Spike_comunicate_define.h"
#include "Jerry_movement.h"
#include "Tom_finder.h"

/*-----------------------------------------------------------*/
comuPara comunicatePara;

int main( void )
{
	comunicatePara.moveQueue = xQueueCreate(1, sizeof(int)*5);
	comunicatePara.moveReady = xSemaphoreCreateBinary();
	comunicatePara.moveSem = xSemaphoreCreateBinary();
	comunicatePara.findQueue = xQueueCreate(1, sizeof(int));
	comunicatePara.findSem = xSemaphoreCreateBinary();
	//comunicatePara.findSem = xSemaphoreCreateMutex();
 
	prvSetupHardware();
	Led_Init();
	SERIAL_USART_Init();
	Jerry_Movemet_Init();
	Tom_Finder_Init();
	
	/* Start the tasks defined within this file/specific to this demo. */
  //xTaskCreate( vCheckTask, "Check", mainCHECK_TASK_STACK_SIZE, NULL, mainCHECK_TASK_PRIORITY, NULL );
	xTaskCreate( vLed_Task, "Led_Task", configMINIMAL_STACK_SIZE/2, NULL, tskIDLE_PRIORITY + 10, NULL );
	xTaskCreate( vJerry_Movement_Task, "Jerry_Movement_Task", configMINIMAL_STACK_SIZE*5, (void *)&comunicatePara, tskIDLE_PRIORITY + 5, NULL);
	xTaskCreate( vTom_Finder_Task, "Tome_Finder_Task", configMINIMAL_STACK_SIZE*15, (void *)&comunicatePara, tskIDLE_PRIORITY + 6, NULL);

	/* Start the scheduler. */
	vTaskStartScheduler();

	/* Will only get here if there was not enough heap space to create the
	idle task. */
	return 0;
}
/*-----------------------------------------------------------*/

